<?php

$path = __DIR__ . '/phpunit-config.php';
if ( file_exists( $path ) )
	require_once( $path );

if ( ! defined( 'SOLISCRM_TEST_PLUGIN_PATH' ) )
	define( 'SOLISCRM_TEST_PLUGIN_PATH', __DIR__ . '/../../wp-content/plugins/soliscrm/soliscrm.php' );

if ( ! defined( 'WP_TESTS_DIR' ) )
	define( 'WP_TESTS_DIR', __DIR__ . '/vendor/wordpress-unit-test/tests/phpunit/' );

$loader = require 'vendor/autoload.php';
$loader->add( 'Solis\\CRM\\Test\\', __DIR__ . '/src' );
$loader->add( 'Solis\\', dirname(SOLISCRM_TEST_PLUGIN_PATH) . '/lib/Solis' );

\Solis\CRM\Test\PHPUnit_Bootstrap::init();