<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test;

/**
 * Bootstrap the PHPUnit test suites
 *
 * @since 0.1
 */
class PHPUnit_Bootstrap {
	/**
	 * Require the core WordPress test suite and make sure the plugin will be
	 * loaded
	 *
	 * @since 0.1
	 */
	public static function init() {
		// Common WP test functions
		require_once( WP_TESTS_DIR . 'includes/functions.php' );

		// Load the plugin
		tests_add_filter( 'muplugins_loaded', array( __CLASS__, 'manually_load_plugin' ) );

		// Bootstrap the WP unit test script
		require( WP_TESTS_DIR . 'includes/bootstrap.php' );
	}

	/**
	 * Load the plugin
	 *
	 * @since 0.1
	 */
	public static function manually_load_plugin() {
		require_once( SOLISCRM_TEST_PLUGIN_PATH );
	}
}