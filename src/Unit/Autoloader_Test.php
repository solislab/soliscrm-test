<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Unit
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Unit;

class Mock_Autoloader_Class extends \Solis\CRM\Plugin\Autoloader {
	private $files = array();

	public function set_files( array $files ) {
		$this->files = $files;
	}

	public function require_file( $file ) {
		return in_array( $file, $this->files );
	}
}

/**
 * Test the PSR-4 autoloader class.
 * @since 0.1
 */
class AutoloaderTest extends \PHPUnit_Framework_TestCase {
	private $loader;

	protected function setUp() {
		$this->loader = new Mock_Autoloader_Class();

		$this->loader->set_files( array(
			'/vendor/foo.bar/src/Class_Name.php',
            '/vendor/foo.bar/src/Doom_Class_Name.php',
            '/vendor/foo.bar/tests/Class_Name_Test.php',
            '/vendor/foo.bardoom/src/Class_Name.php',
            '/vendor/foo.bar.baz.dib/src/Class_Name.php',
            '/vendor/foo.bar.baz.dib.zim.gir/src/Class_Name.php',
		) );

		$this->loader->add_namespace(
			'Foo\Bar',
			'/vendor/foo.bar/src'
		);

		$this->loader->add_namespace(
			'Foo\Bar',
			'/vendor/foo.bar/tests'
		);

		$this->loader->add_namespace(
			'Foo\Bar_Doom',
			'/vendor/foo.bardoom/src'
		);

		$this->loader->add_namespace(
			'Foo\Bar\Baz\Dib',
			'/vendor/foo.bar.baz.dib/src'
		);

		$this->loader->add_namespace(
			'Foo\Bar\Baz\Dib\Zim\Gir',
			'/vendor/foo.bar.baz.dib.zim.gir/src'
		);
	}

	public function test_load_existing_class() {
		$actual = $this->loader->load_class( 'Foo\Bar\Class_Name' );
		$expect = '/vendor/foo.bar/src/Class_Name.php';
		$this->assertSame( $expect, $actual );
	}

	public function test_load_existing_class_from_fallback_path() {
		$actual = $this->loader->load_class( 'Foo\Bar\Class_Name_Test' );
		$expect = '/vendor/foo.bar/tests/Class_Name_Test.php';
		$this->assertSame( $expect, $actual );
	}

	public function test_missing_class() {
		$actual = $this->loader->load_class( 'No_Vendor\No_Package\No_Class' );
		$this->assertFalse( $actual );
	}

	public function test_deep_class() {
		$actual = $this->loader->load_class( 'Foo\Bar\Baz\Dib\Zim\Gir\Class_Name' );
		$expect = '/vendor/foo.bar.baz.dib.zim.gir/src/Class_Name.php';
		$this->assertSame( $expect, $actual );
	}

	public function test_confusion() {
		$actual = $this->loader->load_class( 'Foo\Bar\Doom_Class_Name' );
		$expect = '/vendor/foo.bar/src/Doom_Class_Name.php';
		$this->assertSame( $expect, $actual );

		$actual = $this->loader->load_class( 'Foo\Bar_Doom\Class_Name' );
		$expect = '/vendor/foo.bardoom/src/Class_Name.php';
		$this->assertSame( $expect, $actual );
	}
}