<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage Unit\Core\Entity
 */

namespace Solis\CRM\Test\Unit\Core\Entity;

use Solis\CRM\Core\Entity\Contact;

/**
 * Test for Contact
 *
 * @since 0.1
 */
class Contact_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * Set Gender should throw an exception when an invalid gender value is
	 * specified.
	 *
	 * @expectedException \InvalidArgumentException
	 *
	 * @since 0.1
	 */
	function test_set_gender_throw_exception() {
		$contact = new Contact;
		$contact->set_gender( 10 );
	}
}