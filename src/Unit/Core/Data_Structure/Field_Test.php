<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage Unit\Core\Data_Structure
 */

namespace Solis\CRM\Test\Unit\Core\Data_Structure;

use Solis\CRM\Core\Data_Structure\Field;

/**
 * Test for Field
 *
 * @since 0.1
 */
class Field_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * Unique ID should be generated for each field
	 *
	 * @since 0.1
	 */
	function test_unique_id_generated_for_each_field() {
		$field = new Field();
		$this->assertFalse( empty( $field->get_id() ), 'Unique ID is not generated for Field object.' );
	}
	/**
	 * Belongs to type returns true when the specified types are a subset of
	 * the field's types
	 *
	 * @since 0.1
	 */
	function test_belong_to_type_only_returns_true_when_passed_subset() {
		$field = new Field();
		$field->set_types( array( 'home', 'voice' ) );

		$this->assertTrue( $field->belongs_to_types( 'home' ) );
		$this->assertTrue( $field->belongs_to_types( array( 'home', 'voice' ) ) );
		$this->assertFalse( $field->belongs_to_types( array( 'voice', 'fax' ) ) );
		$this->assertFalse( $field->belongs_to_types( 'fax' ) );
	}
}