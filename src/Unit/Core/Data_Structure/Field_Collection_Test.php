<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage Unit\Core\Data_Structure
 */

namespace Solis\CRM\Test\Unit\Core\Data_Structure;
use Solis\CRM\Core\Data_Structure\Field_Collection;
use Solis\CRM\Core\Data_Structure\Field;

/**
 * Test for Field_Collection
 *
 * @since 0.1
 */
class Field_CollectionTest extends \PHPUnit_Framework_TestCase {
	private static $fields = array();
	private static $collection;

	public static function setUpBeforeClass() {
		self::$collection = new Field_Collection();
		$field_types = array(
			array( 'home' ),
			array( 'home', 'voice' ),
			array( 'work' ),
			array( 'work', 'text' ),
			array( 'home', 'voice', 'fax' ),
			array( 'work', 'voice', 'fax' ),
		);

		foreach ( $field_types as $types ) {
			$field = new Field;
			$field->set_types( $types );
			self::$fields[] = $field;
		}

		foreach ( self::$fields as $field ) {
			self::$collection->add( $field );
		}
	}

	/**
	 * Find fields by types
	 *
	 * @since 0.1
	 */
	function test_find_fields_by_type() {
		$actual = self::$collection->find_by_type( 'home' );
		$expected = array(
			self::$fields[0],
			self::$fields[1],
			self::$fields[4],
		);
		$this->assertEquals( $expected, $actual );

		$actual = self::$collection->find_by_type( 'voice' );
		$expected = array(
			self::$fields[1],
			self::$fields[4],
			self::$fields[5]
		);
		$this->assertEquals( $expected, $actual );

		$actual = self::$collection->find_by_type( array( 'voice', 'fax' ) );
		$expected = array(
			self::$fields[4],
			self::$fields[5],
		);
		$this->assertEquals( $expected, $actual );
	}

	/**
	 * Test remove field
	 *
	 * @since 0.1
	 */
	function test_remove_field() {
		self::$collection->remove( self::$fields[3] );

		$actual = self::$collection->find_by_type( 'work' );
		$expected = array(
			self::$fields[2],
			self::$fields[5]
		);
		$this->assertEquals( $expected, $actual );

		$actual = self::$collection->find( self::$fields[3]->get_id() );
		$this->assertEmpty( $actual );
	}
}