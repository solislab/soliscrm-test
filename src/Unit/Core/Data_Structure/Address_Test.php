<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage Unit\Core\Data_Structure
 */

namespace Solis\CRM\Test\Unit\Core\Data_Structure;

use Solis\CRM\Core\Data_Structure\Address;

/**
 * Test for Address
 *
 * @since 0.1
 */
class Address_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * Exception should be thrown for invalid country code length when using
	 * the setter function
	 *
	 * @expectedException \InvalidArgumentException
	 *
	 * @since 0.1
	 */
	function test_invalid_country_code_in_setter() {
		$address = new Address;
		$address->set_country_code( 'USA' );
	}
}