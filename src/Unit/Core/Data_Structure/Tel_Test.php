<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test
 * @subpackage Unit\Core\Data_Structure
 */

namespace Solis\CRM\Test\Unit\Core\Data_Structure;

use Solis\CRM\Core\Data_Structure\Tel;

/**
 * Test for Tel
 *
 * @since 0.1
 */
class Tel_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * Illegal characters are stripped from the national number
	 *
	 * @since 0.1
	 */
	function test_illegal_characters_in_national_number() {
		$phone_number = new Tel();
		$phone_number->set_national_number( '12 3456-7890' );

		$actual = $phone_number->get_national_number();
		$expected = '1234567890';
		$this->assertEquals( $expected, $actual );
	}

	/**
	 * Illegal characters are stripped from global code
	 *
	 * @since 0.1
	 */
	function test_illegal_characters_in_global_code() {
		// test global code set by constructor
		$phone_number = new Tel();
		$phone_number->set_global_code( '+81 ' );

		$actual = $phone_number->get_global_code();
		$expected = '81';
		$this->assertEquals( $expected, $actual );
	}
}