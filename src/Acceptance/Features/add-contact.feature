Feature: Add Contact
	As an administrator
	I want to manually add contacts
	Because I can use this contact information when dealing with a customer

Scenario: Add customer screen
	Given I am logged in as "admin"
	When I go to Backend -> Contacts -> Add New
	Then I should get a form where I can enter contact information