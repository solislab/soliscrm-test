<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Acceptance
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Acceptance;

use Behat\Behat\Context\BehatContext,
	Behat\Gherkin\Node\PyStringNode,
	Behat\Gherkin\Node\TableNode;

/**
 * Core context class.
 *
 * @since 0.1
 */
final class Context extends \Behat\MinkExtension\Context\RawMinkContext {
	/**
	 * Setup subcontexts, Mink and drivers.
	 *
	 * @since 0.1
	 * @param array $params Parameters from behat.yml
	 */
	public function __construct( $params = array() ) {
		// Mink subcontexts for basic steps
		$this->useContext( 'mink', new \Behat\MinkExtension\Context\MinkContext( $params ) );

		// Common abstract steps
		$this->useContext( 'common', new Subcontext\Common( $params ) );

		// Subcontexts for user authentication steps
		$this->useContext( 'authenticate', new Subcontext\Authenticate( $params ) );

		// Subcontexts for contact input steps
		$this->useContext( 'contact_input', new Subcontext\Contact_Input( $params ) );

		// Initialize Goutte driver
		$goutte_driver = new \Behat\Mink\Driver\GoutteDriver();

		// Initialize Mink
		$mink = new \Behat\Mink\Mink();
		$mink->registerSession( 'goutte', new \Behat\Mink\Session( $goutte_driver ) );
		$this->setMink( $mink );
	}
}