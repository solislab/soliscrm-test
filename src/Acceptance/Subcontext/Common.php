<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Acceptance
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Acceptance\Subcontext;

/**
 * Common steps, preconditions and postconditions
 */
final class Common extends Base {
    /**
     * Human readable locations that point to URL
     * @var string[]
     * @since 0.1
     */
    private $location = array(
            'Backend' => array(
                'Dashboard' => '',
                'Contacts -> Add New' => 'post-new.php?post_type=soliscrm_contact',
            ),
            'Frontend' => array(
                'Login' => 'wp-login.php',
            )
        );

    /**
     * Open specified page
     *
     * @Given /^(?:|I )am on (?P<area>(Backend|Frontend)) -> (?P<page>.+)$/
     * @When /^(?:|I )go to (?P<area>(Backend|Frontend)) -> (?P<page>.+)$/
     */
    public function view( $area, $page ) {
        $url = $this->get_url_for_area( $area );
        $url .= ( isset( $this->location[$area][$page] ) ) ? $this->location[$area][$page] : $page;
        $this->getSession()->visit( $this->locatePath( $url ) );
    }

    /**
     * Checks, that current page PATH is equal to specified.
     *
     * @Then /^(?:|I )should be on (?P<area>(Backend|Frontend)) -> (?P<page>.+)$/
     */
    public function assert_page_address( $area, $page )
    {
        $url = $this->get_url_for_area( $area );
        $url .= ( isset( $this->location[ $area ][ $page ] ) ) ? $this->location[ $area ][ $page ] : $page;
        $this->assertSession()->addressEquals( $this->locatePath( $url ) );
    }
}