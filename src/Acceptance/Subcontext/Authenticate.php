<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Acceptance
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Acceptance\Subcontext;

use Behat\Behat\Context\Step\Given,
	Behat\Behat\Context\Step\When,
	Behat\Behat\Context\Step\Then,
	Behat\Behat\Exception\PendingException;

/**
 * Steps to log in and log out
 *
 * @since 0.1
 */
class Authenticate extends \Behat\Behat\Context\BehatContext {
	/**
	 * Admin username
	 * @since 0.1
	 * @var string
	 */
	private $username;

	/**
	 * Admin password
	 * @since 0.1
	 * @var string
	 */
	private $password;

	/**
	 * Set admin username and password
	 *
	 * @since 0.1
	 * @param array $params Parameters from behat.yml
	 */
	public function __construct( $params = array() ) {
		// admin username and passwords are passed via behat-custom.yml
		$this->username = $params['admin_username'];
		$this->password = $params['admin_password'];
	}

	/**
	 * @Given /^I am logged in as "([^"]*)"$/
	 *
	 * @since 0.1
	 */
	public function log_in( $username ) {
		return array(
			new Given( 'I am on Frontend -> Login' ),
			new When( 'I fill in "log" with "' . $this->username . '"' ),
			new When( 'I fill in "pwd" with "' . $this->password . '"' ),
			new When( 'I press "wp-submit"' ),
			new Then( 'I should be on Backend -> Dashboard' ),
			new Then( 'the response status code should be 200' )
		);
	}
}