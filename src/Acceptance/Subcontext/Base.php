<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Acceptance
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Acceptance\Subcontext;

/**
 * Base class for subcontexts
 *
 * @since 0.1
 */
class Base extends \Behat\MinkExtension\Context\RawMinkContext {
	/**
	 * @inheritdoc
	 * @since  0.1
	 */
	public function getSession( $driver = '' ) {
		return $this->getMainContext()->getSession( $driver );
	}

	/**
	 * Translate human readable area name into base uri.
	 *
	 * @since  0.1
	 * @param  string $area Area name. Backend or Frontend
	 * @return string
	 */
    protected function get_url_for_area( $area ) {
        return ( $area == 'Backend' ) ? '/wp-admin/' : '/';
    }
}