<?php
/**
 * SolisCRM plugin acceptance test suite
 *
 * A Behat acceptance test suite for SolisCRM.
 *
 * @package Solis\CRM\Test\Acceptance
 * @subpackage FeatureContext
 */

namespace Solis\CRM\Test\Acceptance\Subcontext;

use Behat\Behat\Context\Step\Given,
    Behat\Behat\Context\Step\When,
    Behat\Behat\Context\Step\Then,
    Behat\Behat\Exception\PendingException;

/**
 * Contact Input steps
 *
 * @since 0.1
 */
final class Contact_Input extends Base {
    /**
     * @Then /^I should get a form where I can enter contact information$/
     */
    public function see_form() {
        return [
            new Then('the response status code should be 200'),
            new Then('I should see "Add New Contact"')
        ];
    }
}